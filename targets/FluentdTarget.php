<?php namespace logging\targets;

use Fluent\Logger\FluentLogger;
use yii\log\Logger;

final class FluentdTarget extends \yii\log\Target
{
    use BaseTargetTrait;

    /**
     * @var string Fluentd host
     */
    public $host = 'localhost';
    /**
     * @var int Fluentd port
     */
    public $port = 24224;
    /**
     * @var array Options for Fluentd client
     */
    public $options = [];
    /**
     * @var string Tag format, available placeholders: %date, %timestamp, %level
     */
    public $tagFormat = 'app.%level';

    /**
     * @var FluentLogger
     */
    private $logger;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->logger = FluentLogger::open($this->host, $this->port, $this->options);
    }

    /**
     * @inheritdoc
     */
    public function export()
    {
        $messages = array_map([$this, 'formatMessage'], $this->messages);
        foreach($messages as $message) {
            $message = json_decode($message, true);
            $this->logger->post($this->createTag($message), $message);
        }
    }

    /**
     * Generating tag name based on $tagFormat
     *`
     * @param $messages
     *
     * @return string
     */
    private function createTag($messages)
    {
        return str_replace(
            ['%date', '%timestamp', '%level'],
            [
                date('Y-m-d H:i:s'),
                time(),
                $messages['severity'],
            ],
            $this->tagFormat
        );
    }
}
