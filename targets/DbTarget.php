<?php namespace logging\targets;

final class DbTarget extends \yii\log\DbTarget
{
    /**
     * @inheritdoc
     */
    public function export()
    {
        foreach ($this->messages as $key => $message) {
            list($text) = $message;

            if (strpos($text, 'INSERT INTO `log` (`level`, `category`, `log_time`, `prefix`, `message`)') === 0) {
                unset($this->messages[$key]);
            }
        }

        parent::export();
    }
}