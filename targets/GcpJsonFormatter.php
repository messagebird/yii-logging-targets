<?php

namespace logging\targets;

final class GcpJsonFormatter implements Formatter
{
    use BaseTargetTrait;

    /** @var string */
    private $prefix;

    /**
     * @inheritdoc
     */
    public function format($message, $application)
    {
        $this->prefix = $application;
        return $this->formatMessage($message);
    }
}
