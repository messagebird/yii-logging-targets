<?php namespace logging\targets;

use yii\base\InvalidConfigException;

final class FileTarget extends \yii\log\FileTarget
{
    /** @var Formatter */
    public $formatter;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if ($this->formatter === null) {
            $this->formatter = new GcpJsonFormatter();
        }

        if (!$this->formatter instanceof Formatter) {
            throw new InvalidConfigException('Formatter must be instance of ' . Formatter::class);
        }
    }

    /**
     * @inheritdoc
     */
    public function formatMessage($message)
    {
        $application = is_callable($this->prefix) ? call_user_func($this->prefix, $message) : $this->prefix;

        return $this->formatter->format($message, $application);
    }
}
