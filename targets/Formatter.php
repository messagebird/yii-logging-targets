<?php

namespace logging\targets;

interface Formatter
{
    /**
     * @param array $message the Yii log message
     * @param string $application
     * @return string
     */
    public function format($message, $application);
}
