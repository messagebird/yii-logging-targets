<?php

namespace logging\targets;

use yii\helpers\VarDumper;
use yii\log\Logger;
use yii\web\Request;

final class LineFormatter implements Formatter
{
    /**
     * @inheritdoc
     */
    public function format($message, $application)
    {
        list($text, $level, $category) = $message;
        $level = Logger::getLevelName($level);
        if (!is_string($text)) {
            // exceptions may not be serializable if in the call stack somewhere is a Closure
            if ($text instanceof \Exception) {
                $text = (string) $text;
            } else {
                $text = VarDumper::export($text);
            }
        }

        $text = str_replace("\n", ' ', $text);

        $traces = [];
        if (isset($message[4])) {
            foreach ($message[4] as $trace) {
                $traces[] = "in {$trace['file']}:{$trace['line']}";
            }
        }

        $prefix = $this->getMessagePrefix();
        return "{$prefix}[$level][$category] $text";
    }

    private function getMessagePrefix()
    {
        if (\Yii::$app === null) {
            return '';
        }

        $request = \Yii::$app->getRequest();
        $ip = $request instanceof Request ? $request->getUserIP() : '-';

        /* @var $user \yii\web\User */
        $user = \Yii::$app->has('user', true) ? \Yii::$app->get('user') : null;
        if ($user && ($identity = $user->getIdentity(false))) {
            $userID = $identity->getId();
        } else {
            $userID = '-';
        }

        /* @var $session \yii\web\Session */
        $session = \Yii::$app->has('session', true) ? \Yii::$app->get('session') : null;
        $sessionID = $session && $session->getIsActive() ? $session->getId() : '-';

        return "[$ip][$userID][$sessionID]";
    }
}
