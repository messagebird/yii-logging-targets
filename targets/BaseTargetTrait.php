<?php

namespace logging\targets;

use Yii;
use yii\log\Logger;
use yii\web;

trait BaseTargetTrait
{
    /**
     * @inheritdoc
     *
     * Encodes the final output in JSON format
     */
    public function formatMessage($message)
    {
        list($text, $level, $category, $timestamp) = $message;

        @list($seconds, $nanosecond) = explode(".", $timestamp);

        $array = [
            'severity'    => strtoupper(self::yiiToGcpLogLevel($level)),
            'application' => $this->prefix,
            'message'     => $this->getBody($text),
            'timestamp' => [
                'seconds' => $seconds,
                'nanos'   => $nanosecond,
            ],
            'category'    => $category,
            'hostname'    => (empty($_SERVER['HOSTNAME']) === false) ? $_SERVER['HOSTNAME'] : $_SERVER['SERVER_NAME'],
        ];

        if (Yii::$app !== null && !is_a(Yii::$app, 'yii\console\Application')) {

            $array = array_merge($array, [
                'ipaddress'   => $this->getIpAddress(),
                'userId'      => $this->getUserId(),
                'uri'         => $this->getUrl(),
                'userAgent'   => $this->getUserAgent(),
                'method'      => $this->getMethod(),
                'bodyParams'  => $this->getBodyParams(),
                'queryParams' => $this->getQueryParams(),
            ]);
        }
        return json_encode($array);
    }

    /**
     * @param string $level
     * @return string
     */
    private static function yiiToGcpLogLevel($level)
    {
        static $levels = [
            Logger::LEVEL_ERROR => 'ERROR',
            Logger::LEVEL_WARNING => 'WARNING',
            Logger::LEVEL_INFO => 'INFO',
            Logger::LEVEL_TRACE => 'DEBUG',
            Logger::LEVEL_PROFILE_BEGIN => 'DEBUG',
            Logger::LEVEL_PROFILE_END => 'DEBUG',
        ];

        return isset($levels[$level]) ? $levels[$level] : 'UNKNOWN';
    }

    /**
     * @param $text
     *
     * @return string|array
     */
    protected function getBody($text)
    {
        if (is_array($text)) {
            return $text;
        }

        // Try to get a nice overview.
        if (is_string($text)) {
            $textJson = \json_decode($text, true);
            if ($textJson) {
                $text = $textJson;
            }
        }

        if (!is_string($text)) {
            // Exceptions may not be serializable if in the call stack
            // somewhere is a Closure
            if ($text instanceof \Throwable || $text instanceof \Exception) {
                $text = (string) $text;
            }
        }

        if (is_string($text)) {
            $text = ltrim(rtrim($text, '"'), '"');
        }

        return $text;
    }

    /**
     * @return null|string
     */
    protected function getUserAgent()
    {
        $request = Yii::$app->getRequest();
        return $request instanceof web\Request ? $request->getUserAgent() : null;
    }

    /**
     * @return null|string
     */
    protected function getUrl()
    {
        $request = Yii::$app->getRequest();
        return $request instanceof web\Request ? $request->getUrl() : null;
    }

    /**
     * @return int|null|string
     * @throws Yii\base\InvalidConfigException
     */
    protected function getUserId()
    {
        /* @var $user Yii\web\User */
        $user = Yii::$app->has('user', true) ? Yii::$app->get('user') : null;
        if ($user && ($identity = $user->getIdentity(false))) {
            return $identity->getId();
        }

        return null;
    }

    /**
     * @return null|string
     */
    protected function getIpAddress()
    {
        $request = Yii::$app->getRequest();
        return $request instanceof web\Request ? $request->getUserIP() : null;
    }

    /**
     * @return null|string
     */
    protected function getMethod()
    {
        $request = Yii::$app->getRequest();
        return $request instanceof web\Request ? $request->getMethod() : null;
    }

    /**
     * @return array|null
     * @throws Yii\base\InvalidConfigException
     */
    protected function getBodyParams()
    {
        $request = Yii::$app->getRequest();
        return $request instanceof web\Request ? $request->getBodyParams() : null;
    }

    /**
     * @return array|null
     */
    protected function getQueryParams()
    {
        $request = Yii::$app->getRequest();
        return $request instanceof web\Request ? $request->getQueryParams(): null;
    }
}
