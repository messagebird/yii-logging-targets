<?php namespace logging\targets;

use yii\base\InvalidConfigException;

final class StreamTarget extends \yii\log\Target
{
    use BaseTargetTrait;

    /**
     * @var string the URL to use. See http://php.net/manual/en/wrappers.php for details.
     */
    public $url;

    /**
     * @inheritdoc
     */
    public function export()
    {
        if (empty($this->url)) {
            throw new InvalidConfigException('No url configured.');
        }

        $text = implode(PHP_EOL, array_map([$this, 'formatMessage'], $this->messages)) . PHP_EOL;
        if (empty($text)) {
            return;
        }

        if (($fp = @fopen($this->url, 'w')) === false) {
            throw new InvalidConfigException("Unable to append to {$this->url}");
        }
        fwrite($fp, $text);
        fclose($fp);
    }
}
